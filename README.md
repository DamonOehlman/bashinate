# bashinate

System provisioning tool using [BASH scripting](http://tldp.org/LDP/abs/html/).

Currently only compatible with Ubuntu based linux systems (base requirements are the `apt` package manager and `upstart`) and primarily tested with Ubuntu 12.04 LTS.

## Work in Progress

This is most definitely a work in progress, and I would only recommend the truly fearless of heart even try using it now :)

## Installation

```
[sudo] wget https://bitbucket.org/DamonOehlman/bashinate/raw/master/bashinate -O bashinate
[sudo] chmod a+x bashinate
```

## Example Usage

If you are feeling adventurous and deploy a few node apps, then it might be worth checking out the [Provisioning a Node Server How-to](https://bitbucket.org/DamonOehlman/bashinate/wiki/howto-nodeserver).

## General Notes

- Bashinate is something I wrote to meet my own needs, both at work and play.  It may or may not meet yours.

- It is not designed as a replacement for chef, puppet, ansible, etc.  It has a fairly narrow focus compared to these tools.

- If you run bashinate as root it will assume "system provisioning mode" and install packages to `/opt/local/`.  If you run it as a normal user it will assume configuring your tools mode and install to `~/.bashinate/install`.

- Building bashinate has been an exercise in learning BASH as much as it has been about making a lightweight system provisioning tool.  If you spot something that could be done better then feel free to let me know via an issue or pull request.

## Tips and Tricks

Check your version of bashinate:

```sh
./bashinate --version
```

Get bashinate to update itself:

```sh
./bashinate update
```

Remove cached scriptlets:

```sh
./bashinate clean
```

Until bashinate does some nice etag checking of cached scriptlets this is a must regularly.  I'm pretty much running the following all of the time:

```sh
./bashinate clean && ./bashinate < system_recipe
```

## LICENSE (MIT)

Copyright (c) 2013 Damon Oehlman damon.oehlman@gmail.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the 'Software'), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.